# TourGuide Project

## Description
TourGuide is a Spring Boot application that serves as a key component of the TripMaster portfolio of applications. TourGuide has experienced rapid growth in user base, with thousands of new users signing up in the past few months. However, this growth has caused performance issues, resulting in slow response times and user complaints. The project aims to optimize the application to handle the increased load and improve overall user experience.

## Features
- Nearby Attractions: Users can view information about attractions located near their current location.
- Recommendations: Users receive recommendations for attractions regardless of their distance from their current location.
- Reward Points: Users earn reward points for visiting attractions.

## Technologies Used
- Java
- Spring Boot
- Gradle


## Installation
1. Clone the repository.
2. Build the project using Gradle: `gradle build`

## Usage
1. Launch the application.
2. Access the application through the provided URL or localhost.

## Endpoints

Index

- **GET** http://localhost:8080/

user's location
  
- **GET** http://localhost:8080/getLocation
    - example:  http://localhost:8080/getLocation?userName=internalUser1

Location of all users
 
- **GET** http://localhost:8080/getAllCurrentLocations


List of 5 provider offers for an attraction
 
- **GET** http://localhost:8080/getTripDeals
    - example: http://localhost:8080/getTripDeals?userName=internalUser1

List of the 5 attractions closest to the user with their locations and the distance between them
 
- **GET** http://localhost:8080/getNearbyAttractions
    - example: http://localhost:8080/getNearbyAttractions?userName=internalUser1

detail of a user
    
- **GET** http://localhost:8080/getUser
    - example:  http://localhost:8080/getUser?userName=internalUser1

detail of user's references
    
- **GET** http://localhost:8080/getUserPreferences
    - example:  http://localhost:8080/getUserPreferences?userName=internalUser1

UpDate of user's references
    
- **PUT** http://localhost:8080/addUserPreferences
    - example:  http://localhost:8080/addUserPreferences?userName=internalUser77
    -  json body
        - example: {
          "attractionProximity": 300,
          "lowerPricePoint": 100,
          "highPricePoint": 600,
          "tripDuration": 6,
          "ticketQuantity": 4,
          "numberOfAdults": 2,
          "numberOfChildren": 1
          }