package tourGuide.utils;

import gpsUtil.location.Location;

import java.util.UUID;

public class UserRecentLocation {

        private UUID userId;
        private Location location;

        public UserRecentLocation(UUID userId, Location location) {
            this.userId = userId;
            this.location = new Location(location.latitude, location.longitude);
        }

        public UUID getUserId() {
            return this.userId;
        }

        public Location getUserLocation() {
            return this.location;
        }
}
