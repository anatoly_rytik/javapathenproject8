package tourGuide.Controller;

import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tourGuide.DTO.UserPreferencesDTO;
import tourGuide.service.TourGuideService;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;
import tourGuide.user.UserReward;
import tourGuide.utils.NearbyAttraction;
import tourGuide.utils.UserRecentLocation;
import tripPricer.Provider;

import javax.validation.Valid;
import java.util.List;

@RestController
public class TourGuideController {

    private Logger log = LoggerFactory.getLogger(TourGuideController.class);
    private final TourGuideService tourGuideService;

    public TourGuideController(TourGuideService tourGuideService) {
        this.tourGuideService = tourGuideService;
    }

    @GetMapping("/")
    public String index() {
        log.info("Index endpoint called");
        return "Greetings from TourGuide!";
    }

    /**
     * This method is used to get the location of a specific user.
     *
     * @param userName This is the name of the user.
     * @return String This returns the location of the user in a JSON format.
     */
    @GetMapping("/getLocation")
    public ResponseEntity<?> getLocation(@RequestParam String userName) {
        log.info("getLocation endpoint called with userName {}", userName);
        User user = tourGuideService.getUser(userName);
        if (user == null) {
            log.error("User not found with userName {}", userName);
            String errorMessage = "User not found";
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
        }
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(user);
        return ResponseEntity.ok(visitedLocation);
    }

    /**
     * This method is used to get nearby attractions for a specific user.
     *
     * @param userName This is the name of the user.
     * @return String This returns nearby attractions in a JSON format.
     */
    @GetMapping("/getNearbyAttractions")
    public ResponseEntity<?> getNearbyAttractions(@RequestParam String userName) {
        log.info("getNearbyAttractions endpoint called with userName {}", userName);
        User user = tourGuideService.getUser(userName);
        if (user == null) {
            log.error("User not found with userName {}", userName);
            String errorMessage = "User not found";
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
        }
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
        List<NearbyAttraction> nearByAttractions = tourGuideService.getNearByAttractions(visitedLocation);
        return new ResponseEntity<>(nearByAttractions, HttpStatus.OK);
    }

    /**
     * This method is used to get the rewards of a specific user.
     *
     * @param userName This is the name of the user.
     * @return String This returns the rewards of the user in a JSON format.
     */
    @GetMapping("/getRewards")
    public ResponseEntity<?> getRewards(@RequestParam String userName) {
        log.info("getRewards endpoint called with userName {}", userName);
        User user = tourGuideService.getUser(userName);
        if (user == null) {
            log.error("User not found with userName {}", userName);
            String errorMessage = "User not found";
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
        }
        List<UserReward> userRewards = tourGuideService.getUserRewards(getUser(userName));
        return new ResponseEntity<>(userRewards, HttpStatus.OK);
    }

    /**
     * This method is used to get all current locations for all users.
     *
     * @return String This returns all current locations in a JSON format.
     */
    @GetMapping("/getAllCurrentLocations")
    public List<UserRecentLocation> getAllCurrentLocations() {
        log.info("getAllCurrentLocations endpoint called");
        List<UserRecentLocation> allUsersLastVisitedLocations = tourGuideService.getAllUsersLastVisitedLocations();
        return allUsersLastVisitedLocations;
    }

    /**
     * This method is used to get trip deals for a specific user.
     *
     * @param userName This is the name of the user.
     * @return String This returns trip deals in a JSON format.
     */
    @GetMapping("/getTripDeals")
    public ResponseEntity<?> getTripDeals(@RequestParam String userName) {
        log.info("getTripDeals endpoint called with userName {}", userName);
        User user = tourGuideService.getUser(userName);
        if (user == null) {
            log.error("User not found with userName {}", userName);
            String errorMessage = "User not found";
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
        }
        List<Provider> providers = tourGuideService.getTripDeals(getUser(userName));
        return new ResponseEntity<>(providers, HttpStatus.OK);
    }

    /**
     * This method is used to edit the user preferences of a specific user.
     *
     * @param userName This is the user name of the user.
     * @param newPreferences This is the new user preferences to be set.
     * @return ResponseEntity This returns the status of the operation along with the updated user preferences in case of success.
     */
    @PutMapping("/addUserPreferences/{userName}")
    public ResponseEntity<?> editUserPreferences(@PathVariable String userName, @Valid @RequestBody UserPreferencesDTO newPreferences) {
        log.info("editUserPreferences endpoint called with userName {}", userName);
        User user = tourGuideService.getUser(userName);
        if (user == null) {
            log.error("User not found with userName {}", userName);
            String errorMessage = "User not found";
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
        }
        user.setUserPreferences(new UserPreferences(newPreferences));
        log.info("User preferences updated for userName {}", userName);
        return new ResponseEntity<>(newPreferences, HttpStatus.OK);
    }

    /**
     * This method is used to get a specific user.
     *
     * @param userName This is the name of the user.
     * @return User This returns the user object.
     */
    private User getUser(String userName) {
        log.info("getUser method called with userName {}", userName);
        return tourGuideService.getUser(userName);
    }
}
